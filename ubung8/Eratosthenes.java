/* Time-stamp: <2012-12-18 12:40:56 damm>
   Quelltextsammlung fuer Informatik I */



public class Eratosthenes {
    // Liste aller Primzahlen < n erzeugen
    public static void sieb ( boolean[] s) {
	int n = s.length;
	s[0] = s[1] = false; // Vorauss.: n > 1
	for ( int k = 2; k < n; k++) 
	    s[k] = true;          
	for ( int k = 2; k*k <= n; k++) // Primzahl k andidat
	    if ( s[k]) { // Zahl noch im Sieb?
		for (int j = k*k; j < n; j += k) 
		    s[j] = false; 
	    }
    }

    // 'brute force'-Loesung: systematische Probedivision
    public static void trivial ( boolean[] s) {
	int n = s.length;
	s[0] = s[1] = false; // Vorauss.: n > 1
	for (int k = 2; k < n; k++) 
	    s[k] = true;
	for (int k = 2; k < n; k++)    // Primzahl k andidat
	    for (int j = 2; j * j <= k; j++) 
		if ( k % j == 0) { // Probedivision
		    s[k] = false;
		    break;		    
		}
    }

    public static void main( String[] args) {
	if ( args.length != 1) {
	    System.out.println("Aufruf: Eratosthenes <zahl groesser 2>");
	    return; 
	}
	int n = Integer.parseInt(args[0]);
	if ( n <= 2) {
	  System.out.println("Wert nicht erlaubt!");
	  return;
      }
      boolean[] tabelle = new boolean[n]; // default-Init. = false
      System.out.println("Brute force-Algorithmus liefert:");
      trivial(tabelle);
      for ( int i = 2; i < n; ++i) 
	  if (tabelle[i] == true)
	      System.out.print( i + " ");
      System.out.println();
      System.out.println("Sieb-Algorithmus liefert:");
      sieb( tabelle);
      for ( int i = 2; i < n; ++i) 
	  if (tabelle[i] == true)
	      System.out.print( i + " ");
      System.out.println();
  }
}
