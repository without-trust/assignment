/* Time-stamp: <2014-01-26 15:42:45 damm>
   Quelltextsammlung fuer Informatik I */



class Summe {
    public static void main (String[] args) {
	int n = args.length;
	if ( n == 0) {
	    System.out.println("Aufruf: Summe <zahl1> ... <zahlN>");
	    return; // verlasse Methode sofort
	}
	int[] z = new int[n];
	for (int i = 0; i < n; i++)
	    z[i] = Integer.parseInt(args[i]);
	int s = 0; 
	for (int i = 0; i < n; i++)
	    s = s + z[i];
	System.out.println("Summe: " + s);
    }
}
