public class Mul {
   public static void main(String[] args) {
      
      //Initialisiere
      int p = 0;
      if (args.length != 2) 
	{
	   System.out.println("usage: java Mul a b");
	   System.exit(1);
	}
      int a = Integer.parseInt(args[0]); // Einlesen der Parameter
      int b = Integer.parseInt(args[1]); //
      
      Boolean v = (a<0^b<0) ? true : false; // Das Produkt von a und b ist negativ, wenn a xor b negativ sind.
      /* ODER
      if (a<0 | b<0) 
	{
	   bool v = true;
	} else {
	   bool v = false;
	}
      */
      
      a = Math.abs(a);
      b = Math.abs(b);
      
      //Iteriere
      while (b!=0) 
	{
	   if(b%2 == 1)
	     p += a;
	   //p += (b%2 == 1) ? a : 0; 
	   a += a;
	   b = b/2;
	}
      
      //Finalisiere
      System.out.println((v) ? -p : p);
      

   }
}
