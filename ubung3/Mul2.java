public class Mul2 {
   public static void main(String[] args) {
      
      //Initialisiere
      int p = 0;
      if (args.length != 2) 
	{
	   System.out.println("usage: java Mul a b");
	   System.exit(1);
	}
      int a = Integer.parseInt(args[0]); // Einlesen der Parameter
      int b = Integer.parseInt(args[1]); //
      Boolean v = (a<0^b<0) ? true : false; // Das Produkt von a und b ist negativ, wenn a xor b negativ sind (unterschiedliche Vorzeichen)
      a = Math.abs(a);
      b = Math.abs(b);
      
      //Iteriere
      while (b!=0) 
	{
	   if(b%2 == 1) { //b ungerade
	     p += a;
	   }
	   a += a;
	   b = b/2;
	}
      System.out.println(p);
      //Finalisiere
      if (v) 
	{
	   p = -p; //ggf. Vorzeichenwechsel
	   
	}
      System.out.println(p);
      

   }
}
