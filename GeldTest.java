class Geld {
	private int euro, cent;
	
	// Konstruktor
	public Geld(int e, int c) {
		euro = e;	//initialisieren mit e
		cent = 0;	//initialisieren
		setcent(c);
	}
	
	private void setcent(int c) {
		euro += (cent + c) / 100;	//addiere Übertrag auf euro
		cent = (cent + c) % 100;	//cent = letzte zwei stellen
	}
	
	
	public boolean equals(Geld g) {
		if(g.getEuro() == euro && g.getCent() == cent) {	//beide beträge müssen übereinstimmen
			return true;
		}
		return false;
	}
	
	public int getEuro() {
		return euro;
	}
	
	public int getCent() {
		return cent;
	}
	
	public void add(Geld g) {
		euro = euro + g.getEuro();	//eurobetrag aufaddieren
		setcent(g.getCent());	//centbetrag hinzufügen
	}
	
	public String toString() {
		return euro + "," + cent/10 + "" + cent%10 + " Euro";	//eurobetrag, komma, erste stelle von cent, zweite stelle von cent
	}
}

public class GeldTest {
	public static void main(String[] args) {
		Geld g = new Geld(24,47);
		System.out.println("g.toString() = " + g.toString());
		Geld g2 = new Geld(73,397);
		System.out.println("g.toString() = " + g2.toString());
		g.add(g2);
		System.out.println(g.toString());
		
		System.out.println("g equals g2: " + g.toString() + " = " + g2.toString() + " <=> " + g.equals(g2) + " = " + g2.equals(g));
		g2.add(g);
		g2.add(g);
		
		Geld g3 = new Geld(101,44);
		System.out.println("g equals g2: " + g.toString() + " = " + g2.toString() + " <=> " + g.equals(g2) + " = " + g2.equals(g));
		System.out.println("g equals g3: " + g.toString() + " = " + g3.toString() + " <=> " + g.equals(g3) + " = " + g3.equals(g));
		
		
	}
}
