/*
 * Calulates the sqare of a given number n by calculating the sum of the first n odd integers.
 * @author Lorenz Glißmann
 */
public class Square {
    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]),result = 0;
        for (int u=1; u < 2*n; u+=2)
	    result += u;
        System.out.println("n^2\t= " + result);
    }
}

