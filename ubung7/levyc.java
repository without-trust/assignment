class Levyc {
    public static void main(String[] args) {
		if(!args[0].matches("[0-9]+")) {
			error();
		}
        int k = Integer.parseInt(args[0]);
        if(k>20 || k<0) {
        	error();
        }
        
        char[] output = rlevyc(k);	//Array mit + und -
        for (char c : output) {
			//System.out.print(c);
		}
		//System.out.println();
		String outputTurtle = levycToTurtle(output);
		System.out.println(outputTurtle.length() + "\n" + outputTurtle);
		
    }
    public static char[] rlevyc(int k) {
		char[] beginning = {'F'};
		//System.out.println(k);
		return rlevyc(beginning, k);
	}
    private static char[] rlevyc(char[] lastlevyc, int k) {
		if (k==0) {
			return lastlevyc;
		}
		return rlevyc(nextlevyc_str(lastlevyc), k-1);
	}
	
    private static char[] nextlevyc_str (char[] lastlevyc) {
		String lastlevyc_str = "";
		for (char c : lastlevyc) {
			lastlevyc_str = lastlevyc_str + c;
		}
		String nextlevyc_str = "+" + lastlevyc_str + "--" + lastlevyc_str + "+";
		//System.out.println("1 ");
		return nextlevyc_str.toCharArray();
    }
    
    //+,- » R,L
    public static String levycToTurtle(char[] levyc) {
		int readindex = 0;
		int readstop = 0;
		for (int i = 0; i<levyc.length;i++) {
			if (levyc[i] == '+') {
				readindex++;
				//System.out.println(readstop);
			} else {
				readstop = levyc.length-readindex;
				break;
			}
		}
		String output = "";

		while (readindex < readstop) {

			if (levyc[readindex] == '+' && levyc[readindex+1] == '+') {	// muss zuerst überprüft werden, weil sonst über readstop hinausgearbeitet werden könnte.
				output += 'R';
				readindex += 2;
			} else {
				if (levyc[readindex] == '-' && levyc[readindex+1] == '-') {
					output += 'L';
					readindex += 2;
				} else {
					if (levyc[readindex] == 'F') {
						output += 'F';
						readindex++;
					} else {
						if ((levyc[readindex] == '+' && levyc[readindex+1] == '-') || (levyc[readindex] == '-' && levyc[readindex+1] == '+')) {
							readindex += 2;
						}
					}
				}
			}
		}
		return output;
    }
    private static void error() {
		System.out.println("usage: Dragon <n>\n       (0 < n < 20)");
		System.exit(1);
	}
}
