class TestTurtle {
	static short direction;	// Richtung
	static int next_x;		// nächster x-Wert
	static int next_y;		// nächster y-Wert

		
	public static void main(String[] args) {
		
		//Einlesen der Informationen
		int count = StdIn.readInt();
		StdIn.readLine();
		char[] str = new char[count];
		for(int i = 0; i < count; i++) {
			str[i] = StdIn.readChar();
			//System.out.println();
		}
		StdIn.readLine();
		
		//Vorbereiten
		double middle = 0.5;	// Koordinatenursprung
		StdDraw.setPenColor(127,127,127);		// Farbe grau
		StdDraw.line(middle,0,middle,1);		// Koordinatensystem
		StdDraw.line(0,middle,1,middle);		// Koordinatensystem
		StdDraw.setPenColor(0,0,0);				// Farbe schwarz
		double factor = 0.5*simmulation(str);	// Skalierung
		
		//Initialisieren
		int x = 0;
		int y = 0;
		next_x = 0;
		next_y = 0;
		direction = 0;	//zeige nach +x
		
		for(char c : str) {		// foreach-Schleife: Gleichbedeutend mit for(int i=0 ; i<str.length(); i++) und str[i] statt c
			switch (c) {
				case 'L':
					turnLeft();
					break;
				case 'R':
					turnRight();
					break;
				case 'F':
					nextCords();
					StdDraw.line(middle+factor*x, middle+factor*y, middle+factor*next_x, middle+factor*next_y);	//FALSCH
					x = next_x;	//Anfangspunkt der nächsten Linie
					y = next_y;
					break;
				default:
					System.out.println("Da stimmt was mit deiner Eingabe nicht!");
			}
			//System.out.println(c);
		}
		
		
	}
	public static double simmulation(char[] str) {
		
		//Initialisieren
		next_x = 0;
		next_y = 0;
		direction = 0;
		int max_coord = 0;	// größte Koordinate
		
		for(char c : str) {
			switch (c) {
				case 'L':
					turnLeft();
					break;
				case 'R':
					turnRight();
					break;
				case 'F':
					nextCords();
					if (Math.abs(next_x) > max_coord) {	// wenn next_x größer ist, wird es übernommen
						max_coord = Math.abs(next_x);
					}
					if (Math.abs(next_y) > max_coord) {	// wenn next_y größer ist, wird es übernommen
						max_coord = Math.abs(next_y);
					}
					break;
				default:
					System.out.println("Da stimmt was mit deiner Eingabe nicht!");
			}
		}
		return 1/(double)max_coord;
	}
	public static void nextCords() {
		switch (direction) {
			case 0:
				next_x++;
				break;
			case 1:
				next_y--;
				break;
			case 2:
				next_x--;
				break;
			case 3:
				next_y++;
				break;
		}
	}
	public static void turnLeft() {	// L
		if(direction == 0) {	// Nur werte 0-3 sind erlaubt
			direction = 3;
		} else {
			direction--;
		}
	}
	public static void turnRight() {	// R
		if(direction == 3) {	// Nur werte 0-3 sind erlaubt
			direction = 0;
		} else {
			direction++;
		}
	}
	
}
