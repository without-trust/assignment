class Dragon {
    public static void main(String[] args) {
        int k = Integer.parseInt(args[0]);
        char[] dragon = new char[(int)Math.pow(2,k+1)-1];
        output = nextDragon(dragon, k);
    }
    public static char[] nextDragon(, int k) {
		if(k==0) {
			char[] output = {'F'};
			return output;
		}
		char[] lastdragon = nextDragon(new char[(int)Math.pow(2,k+1)-1],k-1);
		char[] output = new char[lastdragon.length()*2+1];
		output[output.length()/2] = 'R';
		for (int i = 0; i< lastdragon.length(); i++) {
			output[i] = lastdragon[i];
			switch (lastdragon[i]) {
				case 'R':
					output[output.length()-i-1] = 'L';
					break;
				case 'L':
					output[output.length()-i-1] = 'R';
					break;
				default:
					output[output.length()-i-1] = 'F';
			}
		}
		return output;
    }
}
