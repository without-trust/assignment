public class Quersumme 
{
   
   
   public static void main(String[] args) 
     {
	
	
	    int number = Integer.parseInt(args[0]), quersumme = 0;
	    while (number != 0)
	  {
	     
	         quersumme = quersumme + number % 10;
	          
	         number = number / 10;
	  }
	
	    System.out.println(quersumme);
     }
   
   
}
