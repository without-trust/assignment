public class GGT {
    public static int euklidKlassisch(int x, int y) {// Wertuebernahme
    System.out.println("|"+"x"+"	|"+"y"+"	|Code");
    System.out.println("|"+x+"	|"+y+"	|"+"euklidKlassisch("+x+", "+y+")");
    System.out.println("|"+x+"	|"+y+"	|"+"while ( "+y+" != 0)");
    while ( y != 0) {// y ungleich 0?
        System.out.println("|"+x+"	|"+y+"	|"+"\tif ( "+x+" > "+y+")");
        if ( x > y) {
            x = x - y;
            System.out.println("|"+x+"	|"+y+"	|"+"\t\tx = "+x+" - "+y);
    }
        else {
        //System.out.println("|"+x+"	|"+y+"	|"+"\telse");
            y = y - x;
        System.out.println("|"+x+"	|"+y+"	|"+"\t\ty = "+y+" - "+x);
    }
    System.out.println("|"+x+"	|"+y+"	|"+"while ( "+y+" != 0)");
    }
    System.out.println("|"+x+"	|"+y+"	|"+"return "+x);
    return x; // Wertauslieferung
}

public static int euklidModern(int x, int y) {
    System.out.println("|"+"x"+"	|"+"y"+"	|Code");
    System.out.println("|"+x+"	|"+y+"	|"+"-"+"	|"+"euklidModern("+x+", "+y+")");
    int r;
    System.out.println("|"+x+"	|"+y+"	|"+"-"+"	|"+"\twhile ( "+y+" != 0)");
    while ( y != 0) {
        r = x % y;
	System.out.println("|"+x+"	|"+y+"	|"+r+"	|"+"\t\tr = "+x+" % "+y);
	System.out.println("|"+x+"	|"+y+"	|"+r+"	|"+"\t\tx = "+y);
        x = y;
        y = r;
	System.out.println("|"+x+"	|"+y+"	|"+r+"	|"+"\t\ty = "+r);
	System.out.println("|"+x+"	|"+y+"	|"+r+"	|"+"\twhile ( "+y+" != 0)");
    }
    System.out.println("|"+x+"	|"+y+"	|"+"-"+"	|"+"\treturn "+x);
    return x; // Wertauslieferung
}

public static int euklidRekursiv (int x, int y) {
    if ( y == 0) {
        return x;
    }
    return euklidRekursiv ( y, x % y);
}

public static void main( String[] args){
    int a = Integer.parseInt(args[0]); // Wertuebernahme von ..
    int b = Integer.parseInt(args[1]); // .. Kommandozeile
    if ( a <= 0 || b <= 0) // '||' steht fuer 'oder'
        System.out.println("nur positive Argumente!!");
    int d;
    d = euklidKlassisch(a,b); // Methodenaufruf
    System.out.println("");
    d = euklidModern(a, b);
    
    System.out.println(d); // Bildschirmausgabe
}


}
