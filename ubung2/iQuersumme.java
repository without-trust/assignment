public class iQuersumme 
{
   public static void main(String[] args) 
     {
	int number = Integer.parseInt(args[0]), quersumme;
	do
	  {
	     quersumme = 0;
	     while (number != 0)
	       {
		  quersumme = quersumme + number % 10;
		  number = number / 10;
	       }
	     number = quersumme;
	  } while (number > 9);
	    System.out.println(quersumme);
     }
}
