/** This class is supposed to test collatz 
 *  @author Lorenz Glißmann
 */
class Collatz2 {
    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        if (args[0].matches("[0-9]+")) {
            for (int i = 0; i < n; i++) {
                if (!collatz(n)) {
                    System.out.println("Dieses n kann mit int nicht überprüft werden.");
                    System.exit(1);
                }
            }
        } else {
            System.out.println("Error: use only positive integral numbers");
            System.exit(1);
        }
    }
    public static Boolean collatz(int a) {
        System.out.printf("%9s =%10s\n", "n", a);
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            if (a % 2 == 1) {
                a = a/2;
            } else {
                if (Integer.MAX_VALUE / 3 - 1 >= a) {
                    a = 3 * a + 1;
                } else {
                    //System.out.println("Error: Int is not big enough to do that");
                    return false;
                }
            }
            System.out.printf("%10s %10s\n", i, a);
            if (a == 1) {
                return true;
            }
        }
        return false;
    }
}
